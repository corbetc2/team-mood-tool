function clearStorage() {
  sessionStorage.clear();
}

function saveSlider(val, displayID, sliderID) {
  console.log(val);
  sessionStorage.setItem(sliderID, val);
  var smileyText;
  switch (val) {
    case "1":
      smileyText = "Strongly Disagree";
      $("#" + displayID).text(smileyText);
      break;
    case "2":
      smileyText = "Disagree";
      $("#" + displayID).text(smileyText);
      break;
    case "3":
      smileyText = "Neutral";
      $("#" + displayID).text(smileyText);
      break;
    case "4":
      smileyText = "Agree";
      $("#" + displayID).text(smileyText);
      break;
    case "5":
      smileyText = "Strongly Agree";
      $("#" + displayID).text(smileyText);
      break;
  }
  sessionStorage.setItem(displayID, smileyText);
}

function mouseDownNeutral(val, displayID, sliderID) {
   console.log("here"+val);
  if (val == 3) {
    sessionStorage.setItem(sliderID, val);
    sessionStorage.setItem(displayID, "Neutral");
    $("#" + displayID).text("Neutral");
  }
}

function displaySlider() {
  $("#sliderA1").val(sessionStorage.getItem("sliderQ1"));
  $("#sliderA2").val(sessionStorage.getItem("sliderQ2"));
  $("#sliderA3").val(sessionStorage.getItem("sliderQ3"));
  $("#sliderA4").val(sessionStorage.getItem("sliderQ4"));
  $("#sliderA5").val(sessionStorage.getItem("sliderQ5"));

  $("#displayA1").text(sessionStorage.getItem("displayQ1"));
  $("#displayA2").text(sessionStorage.getItem("displayQ2"));
  $("#displayA3").text(sessionStorage.getItem("displayQ3"));
  $("#displayA4").text(sessionStorage.getItem("displayQ4"));
  $("#displayA5").text(sessionStorage.getItem("displayQ5"));
}

var smileyComplete = false;

function saveSmiley(resultID) {
  $(".smileyButton").css("background-color", "");
  smileyComplete = true;
  sessionStorage.setItem("displayFaceResult", resultID);
  $("#" + resultID).css("background-color", "rgba(0, 125, 186, 0.2)");
}

function displaySmiley() {
  faceResult = sessionStorage.getItem("displayFaceResult");

  $("#smileyResult").attr("src", "images/" + faceResult + ".png");

  switch (faceResult) {
    case "verySad":
      smileyNumber = "1";
      break;
    case "sad":
      smileyNumber = "2";
      break;
    case "neutral":
      smileyNumber = "3";
      break;
    case "happy":
      smileyNumber = "4";
      break;
    case "veryHappy":
      smileyNumber = "5";
      break;
  }
  $("#smileyNumber").text(smileyNumber + " / 5");
}

function saveComments(val) {
  sessionStorage.setItem("comments", val);
}

function displayComments() {
  $("#displayComments").text(sessionStorage.getItem("comments"));
}

function displayResults() {
  displaySmiley();
  displaySlider();
  displayComments();
}

function submitConfirm() {
  if (smileyComplete == false) {
    window.alert("Please select your smiley rating :)");
    return false;
  }
}
